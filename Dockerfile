FROM python:3.10-slim as build

ENV DEBIAN_FRONTEND noninteractive

ENV FLYWHEEL="/flywheel/v0"
RUN mkdir -p ${FLYWHEEL}
WORKDIR ${FLYWHEEL}

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        "software-properties-common" \
        "build-essential" \
        "apt-transport-https" \
        "ca-certificates" \
        "gnupg" \
        "software-properties-common" \
        "ninja-build" \
        "git" \
        "zlib1g-dev" \
        "cmake" \
        "xvfb" \
        "tcsh" \
        "gcc" \
        "g++" \
        "libeigen3-dev" \
        "zlib1g-dev" \
        "libgl1-mesa-dev" \
        "libfftw3-dev" \
        "libtiff5-dev" \
        "libxt6" \
        "libxcomposite1" \
        "libfontconfig1" \
        "libasound2" \
        "dpkg" \
        "mercurial" \
        "subversion" \
        "curl" \
        "libcurl4" \
        "zip" \
        "bc" \
        "unzip" \
        "perl-modules" \
        "xcb" \
        "qtbase5-dev" \
        "qtchooser" \
        "qt5-qmake" \
        "qtbase5-dev-tools" && \
    rm -rf /var/lib/apt/lists/* 
    
# Install Freesurfer 7.4.1, exclude cuda and trctrain
RUN curl -fsSL --retry 5 https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/7.4.1/freesurfer-linux-ubuntu20_amd64-7.4.1.tar.gz | tar -xz -C /opt/ --strip-components 1 \
         --exclude='freesurfer/lib/cuda' \ 
         --exclude='freesurfer/trctrain' 

# Some of these environment variables are not used for recon-all, however will still set them
# Set the diplay env variable for xvfb
ENV DISPLAY :50.0
ENV QT_QPA_PLATFORM xcb
ENV PATH /opt/freesurfer/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/freesurfer/fsfast/bin:/opt/freesurfer/tktools:/opt/freesurfer/mni/bin:/sbin:/bin:/opt/ants/bin
ENV FS_LICENSE /opt/freesurfer/.license

ENV OS Linux
ENV FS_OVERRIDE 0
ENV FSL_OUTPUT_FORMAT nii.gz
ENV MNI_DIR /opt/freesurfer/mni
ENV LOCAL_DIR /opt/freesurfer/local
ENV FSFAST_HOME /opt/freesurfer/fsfast
ENV MINC_BIN_DIR /opt/freesurfer/mni/bin
ENV MINC_LIB_DIR /opt/freesurfer/mni/lib
ENV MNI_DATAPATH /opt/freesurfer/mni/data
ENV FMRI_ANALYSIS_DIR /opt/freesurfer/fsfast
ENV PERL5LIB /opt/freesurfer/mni/lib/perl5/5.8.5
ENV MNI_PERL5LIB /opt/freesurfer/mni/lib/perl5/5.8.5
ENV XAPPLRESDIR /opt/freesurfer/MCRv97/X11/app-defaults
ENV MCR_CACHE_DIR /flywheel/v0/output/.mcrCache9.7
ENV MCR_CACHE_ROOT /flywheel/v0/output

ENV FREESURFER_HOME /opt/freesurfer

# Remove these unused files from Freesurfer home
RUN rm -rf ${FREESURFER_HOME}/models/* \
    && rm -rf ${FREESURFER_HOME}/fsfast/* \
    && rm -rf ${FREESURFER_HOME}/diffusion/*

# copy in and install fw petsurfer-coreg
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt
COPY run.py ${FLYWHEEL}/
COPY fw_gear_petsurfer_coreg ${FLYWHEEL}/fw_gear_petsurfer_coreg

LABEL org.nrg.commands="[{\"name\": \"petsurfer-coreg\", \"label\": \"petsurfer-coreg\", \"description\": \"Runs petsurfer-coreg v1.0\", \"version\": \"1.0\", \"schema-version\": \"1.0\", \"image\": \"registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/petsurfer-coreg:1.0\", \"type\": \"docker\", \"command-line\": \"python run.py --input_dir /input --output_dir /output --fs freesurferator #FSLICENSE# #PROJ# #SESSIONLABEL# #SLICES# #VIEW# #THREADS#\", \"mounts\": [{\"name\": \"in\", \"writable\": false, \"path\": \"/input\"}, {\"name\": \"out\", \"writable\": true, \"path\": \"/output\"}], \"environment-variables\": {}, \"ports\": {}, \"inputs\": [{\"name\": \"freesurfer_license_file\", \"description\": \"Freesurfer license file name\", \"type\": \"string\", \"default-value\": \"license.txt\", \"required\": true, \"replacement-key\": \"#FSLICENSE#\", \"command-line-flag\": \"--freesurfer_license_file\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"PROJECT\", \"description\": \"Project ID\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#PROJ#\", \"command-line-flag\": \"--project_id\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"SESSIONLABEL\", \"description\": \"session label\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#SESSIONLABEL#\", \"command-line-flag\": \"--exp_id\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"freeview_slices\", \"description\": \"Slices for Freeview, per every slice one screenshot will be created and converted into a gif with the rest of the screenshots.\", \"type\": \"string\", \"default-value\": \"'[50 50 40], [50 50 50], [50 50 60], [50 50 70], [50 50 80]'\", \"required\": true, \"replacement-key\": \"#SLICES#\", \"command-line-flag\": \"--freeview_slices\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"freeview_view\", \"description\": \"View for the freeview screenshot.\", \"type\": \"select-one\", \"default-value\": \"coronal\", \"required\": true, \"replacement-key\": \"#VIEW#\", \"command-line-flag\": \"--freeview_view\", \"command-line-separator\": \" \", \"select-values\": [\"coronal\", \"sagittal\", \"axial\"]}, {\"name\": \"threads\", \"description\": \"Number of threads for coregistration\", \"type\": \"number\", \"default-value\": \"8\", \"required\": false, \"replacement-key\": \"#THREADS#\", \"command-line-flag\": \"--threads\", \"command-line-separator\": \" \", \"select-values\": []}], \"outputs\": [], \"xnat\": [{\"name\": \"petsurfer-coreg\", \"description\": \"Runs petsurfer-coreg v1.0\", \"contexts\": [\"xnat:imageSessionData\"], \"external-inputs\": [{\"name\": \"session\", \"description\": \"Input session\", \"type\": \"Session\", \"required\": true, \"provides-files-for-command-mount\": \"in\", \"provides-value-for-command-input\": \"SESSIONLABEL\", \"load-children\": true}], \"derived-inputs\": [{\"name\": \"project-id\", \"description\": \"Project id\", \"type\": \"string\", \"required\": false, \"provides-value-for-command-input\": \"PROJECT\", \"user-settable\": false, \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"derived-from-xnat-object-property\": \"project-id\", \"multiple\": false}], \"output-handlers\": []}], \"reserve-memory\": 2000, \"limit-cpu\": 8, \"container-labels\": {}, \"generic-resources\": {}, \"ulimits\": {}, \"secrets\": []}]"