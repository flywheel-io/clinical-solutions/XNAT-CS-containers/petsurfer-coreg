import xnat
import os
import sys
from .xnat_logging import stderr_log,stdout_log

def get_freesurfer_license(session,project:str,fs_license:str):
    """ Get Freesurfer license from project resource and save to Freesurfer home
    session: xnat.session.XNATSession
        connection object for XNATpy
    project : str
        project id
    fs_license : str
        Freesurfer license file name

    """
    freesurfer_home=os.environ["FREESURFER_HOME"]

    proj_resources=session.get_json(f"/data/projects/{project}/files")
    filenames = [fs_license]
    found=False

    # look for the specified file(s) in project resource files
    for result in proj_resources['ResultSet']['Result']:
        if result['Name'] in filenames:
            file_uri=result['URI']
            if file_uri:
                session.download(file_uri, f"/{freesurfer_home}/.license")
                
                if os.path.exists(f"/{freesurfer_home}/.license"):
                    stdout_log.info(f"Found and copied {result['Name']} to {freesurfer_home}")
                    found=True
                else:
                    stdout_log.error(f"Unable to copy {result['Name']} to {freesurfer_home}")
                    sys.exit(1)

    if not found:
        stdout_log.error(f"Unable to find {fs_license} under project resources!")
        sys.exit(1)


def get_petmc_files(input_dir="/input/SCANS"):
    """Find all motion corrected PET files across all scans in the session
    Args:
        input_dir (str, optional): path that contains all scans. Defaults to "/input/SCANS".
    Returns:
        petmc_files (dict): dict of full paths to all motion-corrected PET files. Scan ids are the key.
    """

    petmc_files={}
    # Get the list of scan folders
    scans_list = [scan for scan in os.listdir(input_dir) if os.path.isdir(os.path.join(input_dir, scan))]

    # Iterate through each scan
    for scanid in scans_list:
        scan_path = os.path.join(input_dir, scanid)
        
        # Check if the scan contains a 'petsurfer-mc' folder
        petsurfer_mc_resource = os.path.join(scan_path, 'petsurfer-mc')
        if os.path.exists(petsurfer_mc_resource):
            # Get the list of files in the 'petsurfer-mc' folder, and if there are files, get the motion-corrected file path
            petsurfer_mc_files= os.listdir(petsurfer_mc_resource)
            if len(petsurfer_mc_files) > 0:
                petmc_file = [f for f in petsurfer_mc_files if f.endswith('_mc.nii.gz')][0]

                petmc_files[scanid] = os.path.join(petsurfer_mc_resource,petmc_file)
        
    return petmc_files