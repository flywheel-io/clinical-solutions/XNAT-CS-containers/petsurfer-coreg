"""Main module."""
import glob
import itertools
import os
import subprocess as sp
import sys

from PIL import Image
from .xnat_logging import stderr_log,stdout_log

join = os.path.join

def make_2D(max_width, padding, folder, output, comp, bg_color):
    """
    Concatenates png images into a 2D montage that can be seen on flywheel GUI for QC
    Function adapted from https://github.com/AureliusGit/Collager
    """

    ## Setting up
    images = []
    images_height = []
    images_cumul_height = []
    images_height.append(padding)

    ## Load images
    image_index = 1
    for image_path in sorted(glob.glob(os.path.join(folder, "*.png"))):
        image = Image.open(image_path)
        images.append((image_index, image_path, image.size[0], image.size[1]))
        image_index += 1
        image.close()

    ## Check for wide images
    for _, _, width, _ in images:
        if (width + padding * 2) > max_width:
            stdout_log.error("Error: List contains images that are too wide!")
            return False

    ## Check for proper compression value
    if comp not in range(0, 10):
        stdout_log.error("Error: Invalid compression value!")
        return False

    ## Generate output height and row y-coordinates
    current_width = padding
    img_h = 0

    for index, _, width, height in images:
        if (current_width + width + padding) > max_width:
            images_height.append(img_h + padding)
            current_width = padding
            img_h = 0
        current_width += width + padding
        if height > img_h:
            img_h = height
        if index == len(images) - 1:
            images_height.append(img_h + padding)

    output_height = sum(images_height)
    images_cumul_height = list(itertools.accumulate(images_height))

    ## Generating the collage
    current_width = padding
    current_height_index = 0
    current_image_index = 0

    stdout_log.info(f"Processing images... there are {image_index} pngs")

    background = Image.new(
        "RGBA",
        (max_width, output_height),
        (bg_color[0], bg_color[1], bg_color[2], bg_color[3]),
    )
    for index, path, width, _ in images:
        if (current_width + width + padding) > max_width:
            current_height_index += 1
            current_width = padding
        image = Image.open(path)
        background.paste(
            image,
            (current_width, images_cumul_height[current_height_index]),
            image.convert("RGBA"),
        )
        image.close()
        current_image_index += 1
        current_width += width + padding
        # Now we can safely delete it, the gif was created already too
        os.remove(path)

    stdout_log.info("Saving QualityControl_montage.png ...")
    if not os.path.exists(output):
        stdout_log.error("Error: Output directory does not exist!")
        return False
    else:
        background.save(
            os.path.join(output, "QualityControl_montage.png"), compress_level=comp
        )
        return True


def make_gif(frame_folder):
    """
    Concatenates png images into a gif that can be seen on flywheel GUI
    """
    # Read the images
    frames = [Image.open(image) for image in sorted(glob.glob(f"{frame_folder}/*.png"))]
    # Create the gif
    if len(frames) > 0:
        stdout_log.info("There are png images, creating gif QualityControl.gif")
        frame_one = frames[0]
        frame_one.save(
            join(frame_folder, "QualityControl.gif"),
            format="GIF",
            append_images=frames,
            save_all=True,
            duration=1000,
            loop=0,
        )
    else:
        stdout_log.error("There are NO png images, will not create QualityControl.gif")
        sys.exit(1)


def fsxvfb_tkregisterfv(subject, gear_inputs, gear_config):
    # Create images and a gif to illustrate the results
    """
    This is a script that runs freeview with arguments like tkregister.
    Not all tkregister functionality is replicated. Only uses LTA files.
    I could not make freeview store the images without looping, it loads
    the program every time making it slow.
    Think if "slices" needs to be passed as a config var.
    """
    output_dir = gear_inputs["output_dir"]
    fs_dir = gear_inputs["fs"]
    slices = gear_config["freeview_slices"]
    fvview = gear_config["freeview_view"]
    petpath = gear_inputs["petfile"]
    petname = os.path.basename(petpath)

    with open(join(output_dir, "tempfile.txt"), "w") as tempfile:
        tempfile.write(f"--hide-3d-slices\n")
        tempfile.write(f"-transform-volume\n")
        tempfile.write(f"-viewport {fvview}\n")
        tempfile.write(
            f"-v {join(fs_dir,subject,'mri','brainmask.mgz')}"
            f":visible=0:name=brainmask.mgz(targ)"
            f":colormap=grayscale {petpath}:name={petname}(mov)"
            f":reg={join(output_dir,'template.reg.lta')}:colormap=grayscale\n"
        )
        tempfile.write(
            f"--surface {join(fs_dir,subject,'surf','lh.white')}:edgecolor=yellow\n"
        )
        tempfile.write(
            f"--surface {join(fs_dir,subject,'surf','rh.white')}:edgecolor=yellow\n"
        )
        for ind, sl in enumerate(slices.split(",")):
            tempfile.write(f"-slice {sl} \n-ss {join(output_dir, f'fig{ind}.png')}\n")
        tempfile.write("-quit\n")

    cmd = (
        f'export XVFB_SCREEN="1024x768x24" && '
        f"export XDG_RUNTIME_DIR={output_dir} && "
        f'fsxvfb freeview -cmd {join(output_dir,"tempfile.txt")}'
    )
    stdout_log.info(f"This command will be used to create the images: {cmd} ")
    fsxvfb_tkregisterfv_sp=sp.run(cmd, shell=True)

    if fsxvfb_tkregisterfv_sp.returncode !=0:
        stdout_log.error("fsxvfb resulted in a non-zero return code, exiting now!")
        sys.exit(1)

    os.remove(join(output_dir, "tempfile.txt"))


def coreg_with_freesurfer(gear_inputs, gear_config):
    """
    Coregisters PET file and T1 using freesurfer tool mri_coreg
    """
    fs_folders=os.listdir(gear_inputs["fs"])

    for folder in fs_folders:
        if os.path.isdir(os.path.join(gear_inputs["fs"],folder, 'mri')):
            subject=folder
            break

    threads=gear_config["threads"]

    os.makedirs(gear_inputs["output_dir"])

    # Check that the gtmseg file is there, that it was generated in the previous step
    gtmfile = join(gear_inputs["input_dir"],"RESOURCES",gear_inputs["fs"], subject, "mri", "gtmseg.mgz")
    stdout_log.info(
        f"Checking if the gtmseg.file is where it should be, checking for: \ns{gtmfile}"
    )
    if not os.path.isfile(gtmfile):
        stdout_log.error(
            f"gtmfile is required and expected in {gtmfile} but not found. "
            f"It should be run in the freesurferator container with the option run_gtmseg set to True."
        )
        sys.exit(1)

    # Specify the SUBJECTS_DIR name
    # os.environ["SUBJECTS_DIR"] = str(gear_inputs["output_dir"])

    # Create and launch the command line for mri_coreg
    stdout_log.info(f"Calling to the mri_coreg function now for {gear_inputs['petfile']} from scan {gear_inputs['scanid']}")
    cmd = (
        f"mri_coreg --threads {threads} --s {subject} --mov {gear_inputs['petfile']} "
        f"--reg {join(gear_inputs['output_dir'],'template.reg.lta')}"
    )
    stdout_log.info(f"Command for coregistration: {cmd} ")
    coreg_sp=sp.run(cmd, shell=True)

    if coreg_sp.returncode !=0:
        stdout_log.error("Coregistration resulted in a non-zero return code, exiting now!")
        sys.exit(1)
    
    # Create images and a gif to illustrate the results
    fsxvfb_tkregisterfv(subject, gear_inputs, gear_config)

    stdout_log.info(
        "Freesurfer's mri_coreg ended succesfully and images were saved to check the quality of the registration."
    )
   
def run(gear_inputs, gear_config):
    """Performs coregistration between the PET file and the T1w file

    Returns:
        Registration file: Coregistered file
    """

    # Do the Freesurfer coregistration
    coreg_with_freesurfer(gear_inputs, gear_config)

    # Do the gif out of the png-s created in the previous step
    # If there was a previous png montage, delete it just in case
    output = join(gear_inputs["output_dir"])  # 'path to output'
    if os.path.isfile(os.path.join(output, "QualityControl_montage.png")):
        os.remove(os.path.join(output, "QualityControl_montage.png"))
    make_gif(join(gear_inputs["output_dir"]))

    # Do the montage out of the png-s created in the previous step
    max_width = 2608  # 'maximum output width'
    padding = 4  # 'gap size between images and edges'
    folder = join(gear_inputs["output_dir"])  # 'paths to images'
    comp = 6  # 'compression level (0-9)'
    bg_color = (255, 255, 255, 255)  # 'background color in RGBA'

    make_2D(max_width, padding, folder, output, comp, bg_color)

    return 0
