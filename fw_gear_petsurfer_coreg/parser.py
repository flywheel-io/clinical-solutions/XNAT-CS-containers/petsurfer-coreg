"""Parser module to parse command line args from command.json"""
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('--input_dir', required=True)
parser.add_argument('--output_dir', required=True)
parser.add_argument('--project_id', required=True)
parser.add_argument('--exp_id', required=True)
parser.add_argument('--fs', required=True)

parser.add_argument('--threads', required=False,default=8)
parser.add_argument('--freeview_view', required=True,default="coronal")
parser.add_argument('--freeview_slices', required=True)
parser.add_argument('--freesurfer_license_file', required=True,default="license.txt")

args=parser.parse_args() 

def parse_config():
    
 # Gear inputs
    gear_inputs = {
        "input_dir":args.input_dir,
        "fs": os.path.join(args.input_dir,"RESOURCES",args.fs),
        "freesurfer_license_file": args.freesurfer_license_file,
        "output_dir": args.output_dir,
        "project_id":args.project_id,
        "exp_id":args.exp_id.split("/")[-1]
    }

    # Format the slices
    slice_list = args.freeview_slices.replace('[', '').replace(']', '').split(', ')
    formatted_slices = ', '.join(slice_list)

    # Gear configs
    gear_config = {
        "freeview_slices": formatted_slices,
        "freeview_view": args.freeview_view,
        "threads":args.threads
    }

    return gear_inputs, gear_config
