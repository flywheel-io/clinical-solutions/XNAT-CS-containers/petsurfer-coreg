# PetSurfer Coregistration

This session-level XNAT container performs co-registration between a motion-corrected PET scan and an anatomical image (obtained via a preceding  Freesurferator container run). In addition to the registration output, a QC GIF and QC montage file are generated which depict the co-registration. This container is based off version `0.2.1_7.3.2` of the [Petsurfer-coreg gear](https://gitlab.com/flywheel-io/scientific-solutions/gears/petsurfer-coreg/-/tree/0.2.1_7.3.2?ref_type=tags), and uses Freesurfer v7.4.1.

- [PetSurfer Coregistration](#petsurfer-coregistration)
  - [Summary](#summary)
    - [Inputs](#inputs)
    - [Config](#config)
    - [Outputs](#outputs)
    - [Citation](#citation)

## Summary
- A Freesurfer license is required to run this container. The license must be saved as a project resource. The container will specifically look for the file provided in the  [Freesurfer_license_file](#inputs) input. The license file can be saved under any project resource folder name.
  - <img src="./images/freesurfer_license_proj_resrc.png" width=25% />
- `Petsurfer-coreg` uses outputs generated by the containers below:
  - [freesurferator](https://gitlab.com/flywheel-io/clinical-solutions/XNAT-CS-containers/freesurferator)
    - Needs to be run with the *run_gtmseg* config enabled. `Petsurfer-coreg` needs the gtmseg.mgz segmentation file that is generated.
  - [petsurfer-mc](https://gitlab.com/flywheel-io/clinical-solutions/XNAT-CS-containers/petsurfer-mc)
    - `Petsurfer-coreg` will look for the motion-corrected that is generated from this container.
- `Petsurfer-coreg` will go through **each scan** in the session and look for the **petsurfer-mc** scan resource, and run coregistration with that scan's motion-corrected PET file. The outputs are then uploaded to that scan as a **petsurfer-coreg** scan resource via a REST API call.
- Coregistration is skipped for a scan if it has a pre-existing **petsurfer-coreg** resource from a previous run. The user would have to delete that resource if they want to re-run the coregistration.
- Performance:
  - These should be modified per user requirements:
    - `reserve-memory` in the command.json was set to 2000 (2GB)
    - `limit-cpu` in the command.json was set to 8
    - *threads* config was set to 8 by default

### Inputs
- *freesurfer_license_file*
  - __Type__: *string*
  - __Default__: *license.txt*
  - __Description__: *FreeSurfer license file saved as a project resource*

### Config
- *freeview_view*
  - __Type__: *Dropdown*
  - __Default__: *coronal*
  - __Description__: *View for the Freeview screenshot, used for the QC montage and gif outputs. Possible options: sagittal, coronal, axial*

  
- *freeview_slices*
  - __Type__: *String*
  - __Default__: *'[50 50 40], [50 50 50], [50 50 60], [50 50 70], [50 50 80]'*.
  - __Description__: *Slices for Freeview, per every slice one screenshot will be created and converted into a gif with the rest of the screenshots. For example, [50 50 40] refers to the slice at the [x-position y-position z-position].*
  - __Note__: *This string config must be encapsulated by quotes.*
  
- *threads*
  - __Type__: *Number*
  - __Default__: *8*
  - __Description__: *Number of threads to use in coregistration.*


### Outputs
- `petsurfer-coreg` scan resource stores these files:
- <img src="./images/output_fs.png" width=25% />

  - *template.reg.lta*: Registration file between the PET and Freesurfer's T1.mgz file.
  - *QualityControl.gif*: Quality control gif of all the slice screenshots (generated from *freeview_slices* config)
  - *QualityControl_montage.png*: Quality control montage of all the slice screenshots

  
### Citation

- Greve, D. N., Svarer, C., Fisher, P. M., Feng, L., Hansen, A. E., Baare, W., ... & Knudsen, G. M. (2014). Cortical surface-based analysis reduces bias and variance in kinetic modeling of brain PET data. Neuroimage, 92, 225-236

- Greve, D. N., Salat, D. H., Bowen, S. L., Izquierdo-Garcia, D., Schultz, A. P., Catana, C., ... & Johnson, K. A. (2016). Different partial volume correction methods lead to different conclusions: An 18 F-FDG-PET study of aging. NeuroImage, 132, 334-343.

