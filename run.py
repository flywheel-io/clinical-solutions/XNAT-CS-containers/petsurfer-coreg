#!/usr/bin/env python
"""The run script"""
import os
import sys
import traceback
import xnat
from fw_gear_petsurfer_coreg.main import run
from fw_gear_petsurfer_coreg.parser import parse_config
from fw_gear_petsurfer_coreg.utils import get_freesurfer_license, get_petmc_files
from fw_gear_petsurfer_coreg.xnat_logging import stderr_log,stdout_log

def main():
    # parse config
    gear_inputs,gear_config = parse_config()
    project_id=gear_inputs["project_id"]
    exp_id=gear_inputs["exp_id"]
    input_dir=gear_inputs["input_dir"]

    # check if freesurferator session resource exists
    if not os.path.exists(gear_inputs["fs"]):
        stdout_log.error(f"{gear_inputs['fs']} session resource not found!")
        sys.exit(1)

    # find all motion corrected PET files in the session
    pet_mc_files=get_petmc_files()
    
    if pet_mc_files:

        host=os.environ["XNAT_HOST"]
        user=os.environ["XNAT_USER"]
        password=os.environ["XNAT_PASS"]

        # use a single xnat session object for all API calls (license download + resource upload)
        with xnat.connect(host, user=user, password=password) as session:
            # get freesurfer license from project resource and copy to Freesurfer Home
            get_freesurfer_license(session,project_id,gear_inputs.get("freesurfer_license_file"))

            # set SUBJECTS_DIR to be Freesurferator directory
            os.environ["SUBJECTS_DIR"]=f"{gear_inputs['fs']}"
            outputdir_tmp = gear_inputs["output_dir"]

            # run coregistration for every motion-corrected PET file found
            for scanid, petfile in pet_mc_files.items():
                gear_inputs["petfile"] = petfile
                gear_inputs["scanid"] = scanid

                scan_resource=f"{outputdir_tmp}/coreg-scan-{scanid}"
                gear_inputs["output_dir"]= scan_resource
                
                # Check if petsurfer-coreg resource already exists, if so, log a message and do nothing
                if os.path.exists(f"/{input_dir}/SCANS/{scanid}/petsurfer-coreg"):
                    stdout_log.error(f"petsurfer-coreg resource already exists for scan {scanid}! Cannot run coregistration! Please delete the petsurfer-coreg resource for this scan if you wish to re-run!")
                
                # otherwise run coregistration
                else:
                    stdout_log.info(f"scan {gear_inputs['scanid']} has a motion-corrected PET file. Attempting coregistration.")
                    return_code = run(gear_inputs,gear_config)

                    # upload the resource files to the appropriate scan via create_resource() method which uses upload_dir()
                    if return_code == 0:
                        session.projects[project_id].experiments[exp_id].scans[scanid].create_resource("petsurfer-coreg",data_dir=scan_resource)   

                print("-----------------------------------------------------------------------------------")
                print("-----------------------------------------------------------------------------------")
                print("-----------------------------------------------------------------------------------")
                print("-----------------------------------------------------------------------------------")
    else:
        stdout_log.error("No motion-corrected files from Petsurfer-mc were found in the session!")
        sys.exit(1)

if __name__ == "__main__":
    try:
        main()
    except Exception as exp:
        traceback_info = traceback.format_exc()
        stdout_log.error("There was an exception: %s Check stderr.log for more details. \n",exp)
        stderr_log.error("Exception: %s %s \n",exp,traceback_info)
        sys.exit(1)